#include <SD.h>

//schreibt etwa jede Millisekunde einen Wert in die Datei
#include <SdFat.h>
#include <Wire.h>
Sd2Card card;
SdVolume volume;
SdFile root;
SdFile file;

void setup(void)
{
  
  Serial.begin (9600);
 
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  
  card.init();
  volume.init(card);
  root.openRoot(volume);
  char name[] = "Logger00.CSV"; // Hier wird eine neue csv-Datei angelegt, durchnummeriert von 00 bis 99
  for (uint8_t i = 0; i < 100; i++)
    {
      name[6] = i/10 + '0';
      name[7] = i%10 + '0';
      if (file.open(root, name, O_CREAT | O_EXCL | O_WRITE)) break;
    }
    
}


void loop(void)
{
  int helligkeit = analogRead(1);
  Serial.println (helligkeit);
  delay(1000);  

for(int kk=0;kk<1;kk++)  //ursprünglich 60

  {
    file.print(millis()); // Anzahl der vergangenen Millisekunden seit Start des arduino
    file.print(','); // das Komma trennt die Werte
    file.print(helligkeit);
    file.print(',');
    file.println(analogRead(2)); // println beendet die Zeile
    delay(1000); // delay nach Bedarf
  }
 
  file.println("sync 1 Stunde");
  digitalWrite(3, HIGH);
  file.sync(); // "sync" schreibt die Werte fest auf die Karte
  digitalWrite(3, LOW);
}
