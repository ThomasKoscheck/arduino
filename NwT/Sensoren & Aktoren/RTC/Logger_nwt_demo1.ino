#include <SD.h>

// schreibt etwa jede Millisekunde einen Wert in die Datei
#include <SdFat.h>
#include <Wire.h>
Sd2Card card;
SdVolume volume;
SdFile root;
SdFile file;

void setup(void)
{
card.init();
volume.init(card);
root.openRoot(volume);
char name[] = "Logger00.CSV"; // Hier wird eine neue csv-Datei angelegt, durchnummeriert von 00 bis 99
for (uint8_t i = 0; i < 100; i++)
{
name[6] = i/10 + '0';
name[7] = i%10 + '0';
if (file.open(root, name, O_CREAT | O_EXCL | O_WRITE)) break;

}
pinMode(2, OUTPUT);
pinMode(3, OUTPUT);
}
void loop(void)
{
for(int kk=0;kk<20;kk++)
{
file.print(millis()); // Anzahl der vergangenen Millisekunden seit Start des arduino
file.print(','); // das Komma trennt die Werte
file.println(analogRead(2)); // println beendet die Zeile
delay(100); // delay nach Bedarf
}
file.println("sync");
digitalWrite(3, HIGH);
file.sync(); // "sync" schreibt die Werte fest auf die Karte
digitalWrite(3, LOW);


}
