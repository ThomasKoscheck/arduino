 
#include <Servo.h> 
 
Servo myservo;

void setup() 
{ 
  pinMode(10, OUTPUT);      // weiße LED
  pinMode(9, OUTPUT);       // grüne Fade-LED
  pinMode(4, OUTPUT);       // rote LED 
  pinMode(3, OUTPUT);       // grüne LED
  pinMode(1, INPUT);        // Taster       
  pinMode(2, INPUT);        // Taster
  
  //myservo.attach(6); 
} 


//---------- Variablen                                  
  unsigned long pos = 0;    
  unsigned long run;
  int pastMillis = 0; 
  int motorValue = 0;  
  
  int brain = 0;
  int tastervar2 = 0;
 
  int brightness = 0;
  int fade = 5;
  int fadetimer = 0; 
  int zeit = 0;
 
 
 
void loop() 
{
 
  millis();
  
//---------- Servo-Ansteuerung  
  /*unsigned long var1 = analogRead(5);
  
  pos = var1/5.69;  
  
  myservo.write(pos); 
*/ 
  
//---------- Warnung bei hohem Widerstand                      
 if (pos > 160)
  {   
    digitalWrite (10, HIGH);
    delay (500);
    digitalWrite (10, LOW);
    delay (500);
  }



//---------- Lichtsignal bei Veränderung des Widerstandes
 if (millis() - pastMillis > 1000) 
   {
     pastMillis = millis();   
     run = pos;
   }   
  
 if (run != pos)
  {
    digitalWrite (10, HIGH); 
    delay(40);
  }
 
if (run == pos && pos < 160)
  {
    digitalWrite (10, LOW);      
    delay(40);
  }
  
  
//---------- Taster auslesen
  int taster = digitalRead(1);   
  digitalWrite (4, taster); 


//---------- Status der grünen LED umschalten
  int tastervar = digitalRead(2);

  if ((tastervar == HIGH)&&(brain==0)) 
    { 
      delay(100);
        if ((tastervar == HIGH)&&(brain==0))  
          {
            digitalWrite(3, HIGH); 
            brain=1;
          } 
    }

//- - - - - - - - - - - - - - - - - - - - -   
  if ((tastervar == LOW)&&(brain==1)) 
    { 
      delay (100);
        if ((tastervar == LOW)&&(brain==1))
          {
            brain=2;
          }
    }
   
//- - - - - - - - - - - - - - - - - - - - -    
  if ((tastervar == HIGH)&&(brain==2)) 
    {
      delay (100);
        if ((tastervar == HIGH)&&(brain==2)) 
          { 
            digitalWrite(3, LOW); 
            brain=3;
          }
    }
     
//- - - - - - - - - - - - - - - - - - - - -
  if ((tastervar == LOW)&&(brain==3)) 
    {   
      delay(100);
        if ((tastervar == LOW)&&(brain==3)) 
          {
            brain=0;
          }
    } 
  
  
//---------- Fade: grüne LED    
  analogWrite(9, brightness); 
 
  if (brightness == 0)
    {
      fadetimer = 0;
      delay(100);
    }
  
  if (fadetimer == 0)
    { 
      brightness = brightness + fade;
      delay(100);
    }
        
  if (fadetimer == 1)
    {
      brightness = brightness - fade;
      delay(100);
    }
     
  if (brightness == 150) 
    {
      fadetimer = 1; 
      delay(100);
    }   
        

//---------- Rot leuchtet alle 10 Sekunden für 5 Sekunden
  if (millis() - zeit > 10000)
    {
      digitalWrite (7, HIGH);
      delay(5000);
      zeit = millis();
      digitalWrite (7, LOW);
    }
} 




