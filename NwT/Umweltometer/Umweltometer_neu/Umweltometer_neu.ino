/*
**Anschlüsse:
**Pin 1: +5V ("power")
**Pin 2: Datenausgang ("data" bzw. DHTPIN)
**Pin 3:  GND
**2K oder 5K oder 10K Widerstand von Pin 2 (data) nach Pin 1 (power)
**NOTE: Tested on Arduino NANO whose I2C pins are A4==SDA, A5==SCL
**Temperatursensor: rot: 5V; schwarz: GND; gelb: A0;
**Thermometer: Analog 0
** 5V Ausgänge: 8;

** Address pins 0,1 & 2 are all permenantly tied high so the address is fixed at 0x3F
** Arduino holt sich die Werte vom DHT11 und gibt sie auf dem Seriellen Monitor aus.
** DHT11= Temperatur und Luftfeuchtigkeitssensor
** -> wenn man auf das "Gitter" schaut, ist links Pin1
*/


// ---- SD-Karten~SETUP ----
#include <SD.h>
File datafile;            //Benennung der File

// ---- DHT11~SETUP ----
#include "DHT.h"                   //von ladyada: https://github.com/adafruit/DHT-sensor-library
#define DHTPIN 2                  // benutzter ARDUINO-Pin   
#define DHTTYPE DHT11            // unser Sensor: DHT11 
DHT dht(DHTPIN, DHTTYPE);

//---- anderes~ETUP ----
#include <Wire.h>
/*##################### Programm zur Erfassung von: Luftfeuchtigkeit, Temperatur, Lichtstärke und Zeit #####################
---------------------------- von Imanuel Eisenbacher & Thomas Koscheck ----------------------------*/

void setup()
{ 
  Serial.begin(9600);        //Einstellung Serial Monitor
  
  dht.begin();        //Startroutine für den DHT11 Sensor
  
//------------------------------------------------------------------------------------------------------------ 
//Startbildschirm_Serial Monitor:  
  Serial.println ("Arduino startet...");
  Serial.println ("Pruefe Speicher...");        //Prüfroutine für SD card über Monitor
  if (!SD.begin(10))        // prüft ob die SD-Karte vorhanden ist
    {
      Serial.println("Initialisierung fehlgeschlagen");    
      return;        // Hält den Sketch an
    }
  Serial.println("System ist bereit");        //Meldung für erfolgreiche Initialisierung   
}  


//Variablen werden definiert: Auflistung wichtigster Variablen
  int maxtemp = -20;
  int mintemp = 100;
  int startvariable = 0;
  int helligkeit = 300;
  int le = 0;
  int zeit = 0;



void loop()

{   
  
//------------------------------------------------------------------------------------------------------------ 
 //Zeiterkennung:
  while(startvariable == 0)        //Informationserkennung
   {    
    int laufzeit = millis()/60000;        //Laufzeiterkennung in Minuten          
       
              
//------------------------------------------------------------------------------------------------------------ 
  //Feuchtigkeits und Temperaturerfassung:
  
  int h = dht.readHumidity();        //Feuchtigkeit  -> benötigt ca. 250 Millisekunden
  float t = dht.readTemperature();        //Temperatur    -> benötigt ca. 250 Millisekunden


  // Checkt, ob die Daten Zahlen sind, falls nicht (NaN: not a number), ist was falsch gelaufen!!!
  if (isnan(t) || isnan(h)) 
     {
     
     Serial.println("Fehler beim Lesen vom DHT-Sensor");
     } 
  
 
//------------------------------------------------------------------------------------------------------------ 
  //Maximal/Minimaltemperatur
  if (maxtemp < t)        //Maximaltemperatur       
     {
      maxtemp = t;
     }
    
  if (mintemp > t)        //Minimaltemperatur       
     {
      mintemp = t;
     }
     
 
//------------------------------------------------------------------------------------------------------------      
  //Helligkeitserkennung(Button)
 
/*if (digitalRead(7) == HIGH && le == 0)
     {
       lcd.setBacklight(LOW);
       le=1;
     }
     
else if (digitalRead(7) == HIGH && le == 1)
     {
       lcd.setBacklight(HIGH);
       le=0;     
     }
  delay(200);
  Serial.print ("le");
  Serial.println (le);
*/

//------------------------------------------------------------------------------------------------------------      
  //Helligkeitserkennung
  int helligkeit = analogRead(1);
  
//------------------------------------------------------------------------------------------------------------ 
  //Serial-Monitorausgabe:
          
          Serial.print("Temp:");        //Temperaturausgabe
          Serial.println(t);        //Temperaturausgabe
          Serial.print("min:");        //minimal-Temperaturausgabe
          Serial.println(mintemp);        //minimal-Temperaturausgabe
          Serial.print("max:");        //maximal-Temperaturausgabe
          Serial.println(maxtemp);      //maximal-Temperaturausgabe
          Serial.print("Zeit:");        //Zeitausgabe
          Serial.println(laufzeit);        //Zeitausgabe          
          Serial.print("Hmdy:");        //Feuchtigkeitsausgabe
          Serial.print(h);        //Feuchtigkeitsausgabe
          Serial.println("%");        //Feuchtigkeitsausgabe
          Serial.print("Helligkeit:");
          Serial.print (helligkeit);
          Serial.println ();
  
//------------------------------------------------------------------------------------------------------------  
  //Schreiben auf die Karte 
  //if (millis()-zeit>60000)
   // {
      datafile = SD.open ("data.csv" , FILE_WRITE);        //legt die CSV-Datei IT.csv an und schreibt den Namen in das File datafile     
      Serial.println ("Schreibe Daten...");
      datafile.print (millis());           //schreibt auf datafile Text mit Zeilenumbruch
      datafile.print (" # ");
      datafile.print (h);
      datafile.print (" # ");
      datafile.print (t);
      datafile.print (" # ");
      datafile.print (maxtemp);
      datafile.print (" # ");
      datafile.print (mintemp);
      datafile.print (" # ");
      datafile.print (helligkeit);
      datafile.println ();
      datafile.close();        //schließt File:
      Serial.println ("Gespeichert");
      //zeit=millis(); 
      delay(60000);        //1 min: 60.000; 10 min: 600.000
   
  //  }
    
  }
  
}



      
    
      
    
  



