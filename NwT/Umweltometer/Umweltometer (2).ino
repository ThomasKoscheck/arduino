#include <LiquidCrystal.h>

/*
**Anschlüsse:
**Pin 1: +5V ("power")
**Pin 2: Datenausgang ("data" bzw. DHTPIN)
**Pin 3: nichts angeschlossen
**Pin 4: GND
**2K oder 5K oder 10K Widerstand von Pin 2 (data) nach Pin 1 (power)
**NOTE: Tested on Arduino NANO whose I2C pins are A4==SDA, A5==SCL
**Temperatursensor: rot: 5V; schwarz: GND; gelb: A0;
**Thermometer: Analog 0
** 5V Ausgänge: 8;

**Befehle:
**lcd.home();                     ->Setzt LCD Cursor auf 0,0
**lcd.setCursor(Spalte, Zeile);   ->Setzt Cursor an den gewünschten Ort
**lcd.setBacklight(HIGH);         ->Steuert Hintergrundbeleuchtung (an)
**lcd.setBacklight(LOW);          ->Steuert Hintergrundbeleuchtung (aus)
**lcd.print("Text");              ->Gibt Text auf LCD wieder
**lcd.print(Variable);            ->Gibt Werte der Variablen auf LCD wieder

** Address pins 0,1 & 2 are all permenantly tied high so the address is fixed at 0x3F
** Arduino holt sich die Werte vom DHT11 und gibt sie auf dem Seriellen Monitor aus.
** DHT11= Temperatur und Luftfeuchtigkeitssensor
** -> wenn man auf das "Gitter" schaut, ist links Pin1
*/

#include "DHT.h"                 //von ladyada: https://github.com/adafruit/DHT-sensor-library

#define DHTPIN 2                 // benutzter ARDUINO-Pin

#define DHTTYPE DHT11            // unser Sensor: DHT11 

DHT dht(DHTPIN, DHTTYPE);

#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

#define I2C_ADDR    0x3F         // Define I2C Address where the PCF8574A is
#define BACKLIGHT_PIN     3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

int n = 1;
LiquidCrystal_I2C	lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);


/*##################### Programm zur Erfassung von: Luftfeuchtigkeit, Temperatur, Lichtstärke und Zeit #####################
---------------------------- von Imanuel Eisenbacher & Thomas Koscheck ----------------------------*/

void setup()
{
  Serial.begin(9600); 
  dht.begin();
     
  //Switch on the backlight:
  lcd.begin(24,4);                               //definiert Display; 24 Stellen, 4 Zeilen
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(HIGH);
  
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
  Serial.begin(9600);

  
  //Startbildschirm:
  lcd.home();
  lcd.setCursor(2,0);
  lcd.print("IT-Display Test");
  lcd.setCursor (1,2);
  lcd.print("Arduino startet...");
  delay(3000);
  lcd.clear();
  delay(500);
}


void loop()
{   
  //Variablen werden definiert:
  int maxtemp = -10;
  int mintemp = 100;
  int startvariable = 0;
  int helligkeit = 300;
  int eingangswert = 0;

 
  while(startvariable == 0)     //Informationserkennung
   {    
    int laufzeit = millis()/60000;     //Laufzeiterkennung in Minuten          
      
    int bildwiederholung = 1;
              
              
  //Erfassen von:
  int h = dht.readHumidity();          //Feuchtigkeit  -> benötigt ca. 250 Millisekunden
  float t = dht.readTemperature();     //Temperatur    -> benötigt ca. 250 Millisekunden


  // Checkt, ob die Daten Zahlen sind, falls nicht (NaN: not a number), ist was falsch gelaufen!!!
  if (isnan(t) || isnan(h)) 
     {
      Serial.println("Fehler beim Lesen vom DHT-Sensor");
     } 
  
  else 
     {
      //t = t - 1.00;  ->Diese Korrektur war bei mir nötig
      Serial.print("Feuchtigkeit:"); 
      Serial.print(h);
      Serial.print(" %\t   ");
      Serial.print("Temperatur:"); 
      Serial.print(t);
      Serial.println(" *C");
     
     }
    
     
  if(maxtemp < t)     //Maximaltemperatur       
     {
      maxtemp = t;
     }
    
  if(mintemp > t)     //Minimaltemperatur       
     {
      mintemp = t;
     }
     
    
  int helligkeit = analogRead (3); 
  if (helligkeit>300)
     {
      lcd.setBacklight(LOW);      //deaktiviert Display, wenn es hell ist
     }
    
  if (helligkeit<300)
     {
      lcd.setBacklight(HIGH);    //aktiviert Display, wenn es dunkel ist
     }
    
      
          lcd.setCursor(0,0);        //Temperaturausgabe
          lcd.print("Temp:");        //Temperaturausgabe
          lcd.setCursor(5,0);        //Temperaturausgabe
          lcd.print(t);              //Temperaturausgabe
          lcd.setCursor(0,1);        //minimal-Temperaturausgabe
          lcd.print("min:");         //minimal-Temperaturausgabe
          lcd.setCursor(4,1);        //minimal-Temperaturausgabe
          lcd.print(mintemp);        //minimal-Temperaturausgabe
          lcd.setCursor(10,1);       //maximal-Temperaturausgabe
          lcd.print("max:");         //maximal-Temperaturausgabe
          lcd.setCursor(14,1);       //maximal-Temperaturausgabe
          lcd.print(maxtemp);        //maximal-Temperaturausgabe
          //lcd.home();
          lcd.setCursor(12,0);       //Zeitausgabe
          lcd.print("Zeit:");        //Zeitausgabe
          lcd.print(laufzeit);       //Zeitausgabe
          lcd.setCursor(0,2);        //Feuchtigkeitsausgabe
          lcd.print("Hmdy:");        //Feuchtigkeitsausgabe
          lcd.setCursor(6,2);        //Feuchtigkeitsausgabe
          lcd.print(h);              //Feuchtigkeitsausgabe
          lcd.setCursor(8,2);        //Feuchtigkeitsausgabe 
          lcd.print("%");            //Feuchtigkeitsausgabe
    
  }
}


      
    
      
    
  


