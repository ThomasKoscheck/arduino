/*
**Anschlüsse:
**Pin 1: +5V ("power")
**Pin 2: Datenausgang ("data" bzw. DHTPIN)
**Pin 3: nichts angeschlossen
**Pin 4: GND
**2K oder 5K oder 10K Widerstand von Pin 2 (data) nach Pin 1 (power)
**NOTE: Tested on Arduino NANO whose I2C pins are A4==SDA, A5==SCL
**Temperatursensor: rot: 5V; schwarz: GND; gelb: A0;
**Thermometer: Analog 0
** 5V Ausgänge: 8;

**Befehle:
**lcd.home();                     ->Setzt LCD Cursor auf 0,0
**lcd.setCursor(Spalte, Zeile);   ->Setzt Cursor an den gewünschten Ort
**lcd.setBacklight(HIGH);         ->Steuert Hintergrundbeleuchtung (an)
**lcd.setBacklight(LOW);          ->Steuert Hintergrundbeleuchtung (aus)
**lcd.print("Text");              ->Gibt Text auf LCD wieder
**lcd.print(Variable);            ->Gibt Werte der Variablen auf LCD wieder

** Address pins 0,1 & 2 are all permenantly tied high so the address is fixed at 0x3F
** Arduino holt sich die Werte vom DHT11 und gibt sie auf dem Seriellen Monitor aus.
** DHT11= Temperatur und Luftfeuchtigkeitssensor
** -> wenn man auf das "Gitter" schaut, ist links Pin1
*/
#include <LiquidCrystal.h>
#include "DHT.h"                 //von ladyada: https://github.com/adafruit/DHT-sensor-library
#define DHTPIN 2                 // benutzter ARDUINO-Pin
#define DHTTYPE DHT11            // unser Sensor: DHT11 
DHT dht(DHTPIN, DHTTYPE);

#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

#define I2C_ADDR    0x3F         // Define I2C Address where the PCF8574A is
#define BACKLIGHT_PIN     3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

#include <SD.h>

int n = 1;
LiquidCrystal_I2C	lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);
File datafile;     //Benennung der File

/*##################### Programm zur Erfassung von: Luftfeuchtigkeit, Temperatur, Lichtstärke und Zeit #####################
---------------------------- von Imanuel Eisenbacher & Thomas Koscheck ----------------------------*/

void setup()
{
  Serial.begin(9600); 
  dht.begin();
     
  //Switch on the backlight:
  lcd.begin(24,4);                               //definiert Display; 24 Stellen, 4 Zeilen
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(HIGH);
  
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT); 
  pinMode(8, OUTPUT);      //Pin 8 wird als Output definiert
  pinMode(9, OUTPUT);      //Pin 9 wird als Output definiert
  pinMode(10, OUTPUT);     //muss auf OUTPUT gestellt sein
  
  //Startbildschirm:
  lcd.home();
  lcd.setCursor (1,0);
  lcd.print("Arduino startet...");
  lcd.setCursor (1,2);
  lcd.print ("Pruefe Speicher...");        //Prüfroutine für SD card über Monitor
  delay(3000);
  if (!SD.begin(10))                // prüft ob die SD-Karte vorhanden ist
    {
      lcd.setCursor (1,2);
      lcd.print ("Karte fehlerhaft");    
      return;              // Hält den Sketch an
    }
     lcd.setCursor (1,2);
     lcd.print("System ist bereit ");   //Meldung für erfolgreiche Initialisierung
   
  delay(6000);
  lcd.clear();
  delay(500);
}


void loop()
{   
  //Variablen werden definiert: Auflistung wichtigster Variablen
  int maxtemp = -10;
  int mintemp = 100;
  int startvariable = 0;
  int helligkeit = 300;
  int eingangswert = 0;
 
//------------------------------------------------------------------------------------------------------------ 
 //Zeiterkennung:
  while(startvariable == 0)     //Informationserkennung
   {    
    int laufzeit = millis()/60000;     //Laufzeiterkennung in Minuten          
      
    int bildwiederholung = 1;
              
//------------------------------------------------------------------------------------------------------------ 
  //Feuchtigkeits und Temperaturerkennung:
  //Erfassen von:
  int h = dht.readHumidity();          //Feuchtigkeit  -> benötigt ca. 250 Millisekunden
  float t = dht.readTemperature();     //Temperatur    -> benötigt ca. 250 Millisekunden


  // Checkt, ob die Daten Zahlen sind, falls nicht (NaN: not a number), ist was falsch gelaufen!!!
  if (isnan(t) || isnan(h)) 
     {
     lcd.setCursor (1,3);
     lcd.println("Fehler beim Lesen vom DHT-Sensor");
     } 
  
  else 
     {
      //t = t - 1.00;  ->Diese Korrektur war bei mir nötig
      Serial.print("Feuchtigkeit:"); 
      Serial.print(h);
      Serial.print(" %\t   ");
      Serial.print("Temperatur:"); 
      Serial.print(t);
      Serial.println(" *C");
     
     }
    
//------------------------------------------------------------------------------------------------------------ 
  //Maximal/Minimaltemperatur
  if (maxtemp < t)     //Maximaltemperatur       
     {
      maxtemp = t;
     }
    
  if (mintemp > t)     //Minimaltemperatur       
     {
      mintemp = t;
     }
   Serial.print("Maxtemp:"); 
   Serial.print(maxtemp); 
   Serial.print("                 Mintemp:"); 
   Serial.print(                  mintemp);   
    
     
//------------------------------------------------------------------------------------------------------------      
  //Helligkeitserkennung:  
  int helligkeit = analogRead (3); 
  if (helligkeit > 300)
     {
      lcd.setBacklight(LOW);      //deaktiviert Display, wenn es hell ist
     }
    
  if (helligkeit<300)
     {
      lcd.setBacklight(HIGH);    //aktiviert Display, wenn es dunkel ist
     }
  Serial.println("Helligkeit:"); 
  Serial.println(helligkeit);    

//------------------------------------------------------------------------------------------------------------ 
  //Displayausgabe:
          lcd.setCursor(0,0);        //Temperaturausgabe
          lcd.print("Temp:");        //Temperaturausgabe
          lcd.setCursor(5,0);        //Temperaturausgabe
          lcd.print(t);              //Temperaturausgabe
          lcd.setCursor(0,1);        //minimal-Temperaturausgabe
          lcd.print("min:");         //minimal-Temperaturausgabe
          lcd.setCursor(4,1);        //minimal-Temperaturausgabe
          lcd.print(mintemp);        //minimal-Temperaturausgabe
          lcd.setCursor(10,1);       //maximal-Temperaturausgabe
          lcd.print("max:");         //maximal-Temperaturausgabe
          lcd.setCursor(14,1);       //maximal-Temperaturausgabe
          lcd.print(maxtemp);        //maximal-Temperaturausgabe
          //lcd.home();
          lcd.setCursor(12,0);       //Zeitausgabe
          lcd.print("Zeit:");        //Zeitausgabe
          lcd.print(laufzeit);       //Zeitausgabe
          lcd.setCursor(0,2);        //Feuchtigkeitsausgabe
          lcd.print("Hmdy:");        //Feuchtigkeitsausgabe
          lcd.setCursor(6,2);        //Feuchtigkeitsausgabe
          lcd.print(h);              //Feuchtigkeitsausgabe
          lcd.setCursor(8,2);        //Feuchtigkeitsausgabe 
          lcd.print("%");            //Feuchtigkeitsausgabe
  
  
//------------------------------------------------------------------------------------------------------------  
  //Vorbereitung des Schreibens auf die Karte 

datafile = SD.open ("IT.csv" , FILE_WRITE);      //legt die CSV-Datei IT.csv an und schreibt den Namen in das File datafile

datafile.println ("Testen...");      //schreibt auf datafile Text mit Zeilenumbruch
datafile.println ("Test erfolgreich");
datafile.println ("Helligkeit:");
datafile.print (helligkeit);
datafile.println ("Feuchtigkeit:"),
datafile.print(h);
datafile.println ("Zeit::"),
datafile.print (laufzeit);
datafile.println ("Temperatur:"),
datafile.println (t);
//schließt File:
datafile.close();

delay(10000);   
  }
}


      
    
      
    
  


