#include <avr/io.h>


const int ledPin = 3;              //Def. LED-PIN auf dem Bord

int ledState = LOW;                //Def. LED-Status
long previousMillis = 0;           //Def. Zeitpunkt des letzten LED-Status-Wechels

long interval =1000;               //Blink-Intervall (Millissekunden)

void setup ()
{
  pinMode (ledPin, OUTPUT);        //LED-PIN wird auf Ausgang gesetzt
  
}
  
void loop ()
{
  unsigned long currentMillis = millis();          //aktueller Wert
  
  if (currentMillis - previousMillis > interval) 
  {
   previousMillis = currentMillis;                  //togglen (Umschalten
   ledState = ((ledState == LOW)? HIGH : LOW);
   digitalWrite (ledPin, ledState);
  }
}
  
  
