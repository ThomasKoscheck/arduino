#include <SD.h>
#include <Wire.h>
#include <RTClib.h>

RTC_DS1307 RTC;

File file;

void setup()
{
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
  
  //------------------------------------------------
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    // uncomment it & upload to set the time, date and start run the RTC!
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  pinMode(3, OUTPUT);
  pinMode(13, OUTPUT);
  
  String name;
  DateTime now = RTC.now();
    
  name+=(now.year(), DEC);
  name+=('/');
  name+=(now.month(), DEC);
  name+=('/');
  name+=(now.day(), DEC);
  name+=(' ');
  name+=(now.hour(), DEC);
  name+=(':');
  name+=(now.minute(), DEC);
  name+=(':');
  
  
  //------------------------------------------------
  Serial.print("Initializing SD card...");
  pinMode(10, OUTPUT);
   
  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  file = SD.open("data" + +".csv", FILE_WRITE);
  string 
  
  // if the file opened okay, write to it:
  if (file) {
    Serial.print("Writing to " + + "...");
    file.println("testing 1, 2, 3.");
	// close the file:
    file.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.csv");
  }
  /*
  // re-open the file for reading:
  file = SD.open("metzler.csv");
  if (file) {
    Serial.println("metzler.txt:");
    
    // read from the file until there's nothing else in it:
    while (file.available()) {
    	Serial.write(file.read());
    }
    // close the file:
    //file.close();
  } else {
  	// if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  */
  
  //------------------------------------------------
   
}

void loop()
{

file = SD.open("metzler.csv", FILE_WRITE);
file.print("test"); // Anzahl der vergangenen Millisekunden seit Start des arduino	
file.close();
}

