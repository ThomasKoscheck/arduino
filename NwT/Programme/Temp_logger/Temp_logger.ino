/*
 created April 2016
 by Thomas Koscheck &
 by Imanuel Eisenbacher
*/
 
#include <RTClib.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
//#include <DHT.h> //von ladyada: https://github.com/adafruit/DHT-sensor-library
//#define DHTPIN 2     // benutzter ARDUINO-Pin
//#define DHTTYPE DHT22   // unser Sensor: DHT22 
#include <cactus_io_AM2302.h>
#define AM2302_PIN 2

RTC_DS1307 RTC;
AM2302 dht(AM2302_PIN);

File myFile;

void setup() {
  
   // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) 
  { } // wait for serial port to connect. Needed for native USB port only
  
  Wire.begin();
  RTC.begin();
  dht.begin();
  
  if (! RTC.isrunning())  {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    // uncomment it & upload to set the time, date and start run the RTC!
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  
  RTC.adjust(DateTime(__DATE__, __TIME__));
 
  Serial.println("Initialisiere SD-Karte...");
  if (!SD.begin(10))  {  //event. auch Port 4  
    Serial.println("Initialisierung fehlgeschlagen");
    return;
  }
  
  Serial.println("Initialisierung erfolgreich.");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("Messung.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to Messung.txt ... ");
    myFile.println("testing 1, 2, 3.");
    // close the file:
    myFile.close();
    Serial.println("done.");
  } 
  else  {
    // if the file didn't open, print an error:
    Serial.println("error opening file");
  }

  /* //re-open the file for reading:
  myFile = SD.open(name);
  if (myFile) 
  {
    Serial.println(name);

    // read from the file until there's nothing else in it:
    while (myFile.available()) 
    {
      Serial.write(myFile.read());
    }    
    // close the file:
    myFile.close();
  }
  else 
  {
    // if the file didn't open, print an error:
    Serial.println("error opening file");   
  }*/
  
 
}

void loop() { 

if (isnan(dht.humidity) || isnan(dht.temperature_C)) {
 Serial.println("AM2302 sensor read failure!");
 return;
  }
  dht.readHumidity();
  dht.readTemperature();

  double luft = analogRead(3);

  DateTime now = RTC.now();
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print("    ");
  Serial.print("Temperatur: ");
  Serial.print(dht.temperature_C);
  Serial.print("     Luftguete: ");
  Serial.print(luft);
  Serial.print("     Feuchtigkeit: ");
  Serial.print(dht.humidity);
  Serial.println();
  
  myFile = SD.open("Messung.txt", FILE_WRITE);
  myFile.print(now.hour(), DEC);
  myFile.print(':');
  myFile.print(now.minute(), DEC);
  myFile.print(", ");
  myFile.print(dht.temperature_C); 
  myFile.print(", ");
  myFile.print(luft);
  myFile.print(", ");
  myFile.print(dht.humidity);
  myFile.println();
  myFile.close();
  
  delay(20000);
}
