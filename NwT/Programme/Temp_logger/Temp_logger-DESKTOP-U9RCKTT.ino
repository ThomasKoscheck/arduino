/*
  SD card read/write

 This example shows how to read and write data to and from an SD card file
 The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4

 created   Nov 2010
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe

 This example code is in the public domain.

 */

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include "RTClib.h"

RTC_DS1307 RTC;

File myFile;

void setup() 
  {
   // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) 
  { } // wait for serial port to connect. Needed for native USB port only
  
  Wire.begin();
  RTC.begin();
  
  if (! RTC.isrunning()) 
  {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    // uncomment it & upload to set the time, date and start run the RTC!
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  
  RTC.adjust(DateTime(__DATE__, __TIME__));
    
  String name;
  DateTime now = RTC.now();
  name += (now.year(), DEC); 
  name += '_'; 
  name += (now.month(), DEC); 
  name += '_'; 
  name += (now.day(), DEC); 
  name += ".csv";
  Serial.println(name);
 
  Serial.println("Initialisiere SD-Karte...");
  if (!SD.begin(10))  //event. auch port 4
  {    
    Serial.println("Initialisierung fehlgeschlagen");
    return;
  }
  
  Serial.println("Initialisierung erfolgreich.");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open(name, FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) 
  {
    Serial.print("Writing to " + name + "...");
    myFile.println("testing 1, 2, 3.");
    // close the file:
    myFile.close();
    Serial.println("done.");
  } 
  else 
  {
    // if the file didn't open, print an error:
    Serial.println("error opening file");
  }

  /*//re-open the file for reading:
  myFile = SD.open(name);
  if (myFile) 
  {
    Serial.println(name);

    // read from the file until there's nothing else in it:
    while (myFile.available()) 
    {
      Serial.write(myFile.read());
    }    
    // close the file:
    myFile.close();
  }
  else 
  {
    // if the file didn't open, print an error:
    Serial.println("error opening file");   
  }*/
}

void loop() {
  Serial.println((analogRead(1)* 0.00488 * 1000)-500)/100;
  delay(2000);
}
