void setup ()
{
  Serial.begin (9600);
  pinMode (12, OUTPUT);
  pinMode (11, OUTPUT);
  pinMode (10, OUTPUT);
  pinMode (9, OUTPUT);
}

void loop ()
{
  float wert = analogRead (5);
  float s = wert*0.00488;

  Serial.println (s);
  delay (500);

  if (s > 4 && s <= 5)
    digitalWrite (12, HIGH);
  digitalWrite (11, HIGH);
  digitalWrite (10, HIGH);
  digitalWrite (9, HIGH);


  if (s >3 && s <= 4)
  {
    digitalWrite (12, LOW);
    digitalWrite (11, HIGH);
    digitalWrite (10, HIGH);
    digitalWrite (9, HIGH);
  }


  if (s >2 && s <= 3)
  {
    digitalWrite (12, LOW);
    digitalWrite (11, LOW);
    digitalWrite (10, HIGH);
    digitalWrite (9, HIGH);
  }

  if (s >= 1 && s <=2)
  {
    digitalWrite (12, LOW);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, HIGH);
  }

  if (s < 1 && s >= 0)
  {
    digitalWrite (12, LOW);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);
  }

  if (s<0.5)
  {
    digitalWrite (12, HIGH); 
    delay (1500);
    digitalWrite (11, HIGH);
    delay (1500);
    digitalWrite (10, HIGH);
    delay (1500);
    digitalWrite (9, HIGH);
    delay (1500); 
    digitalWrite (9, LOW); 
    digitalWrite (10, LOW);
    digitalWrite (11, LOW);
    digitalWrite (12, LOW);
  }
  
   if (s>4.5)
  {
    digitalWrite (12, HIGH); 
    delay (50);
    digitalWrite (11, HIGH);
    delay (50);
    digitalWrite (10, HIGH);
    delay (50);
    digitalWrite (9, HIGH);
    delay (50); 
    digitalWrite (12, LOW); 
    digitalWrite (11, LOW);
    digitalWrite (10, LOW); 
    digitalWrite (9, LOW);   
   
  }
} 

