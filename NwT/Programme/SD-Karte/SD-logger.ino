
/*
  SD card read/write
 
 This example shows how to read and write data to and from an SD card file 	
 The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 
 created   Nov 2010
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.
 	 
 */
 
 
 
 
#include <SD.h>
//#include <SdFat.h>
#include <Wire.h>
//Sd2Card card;
//SdVolume volume;
//SdFile root;
//SdFile file;
File file;

void setup()
{
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(10, OUTPUT);
   
  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  file = SD.open("metzler.csv", FILE_WRITE);
  
  // if the file opened okay, write to it:
  if (file) {
    Serial.print("Writing to metzler.txt...");
    file.println("testing 1, 2, 3.");
	// close the file:
    file.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.csv");
  }
  /*
  // re-open the file for reading:
  file = SD.open("metzler.csv");
  if (file) {
    Serial.println("metzler.txt:");
    
    // read from the file until there's nothing else in it:
    while (file.available()) {
    	Serial.write(file.read());
    }
    // close the file:
    //file.close();
  } else {
  	// if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  */
}

void loop()
{

file = SD.open("metzler.csv", FILE_WRITE);
file.print("test"); // Anzahl der vergangenen Millisekunden seit Start des arduino	
file.close();
}

