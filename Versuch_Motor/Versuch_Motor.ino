
//Right motor
#define logicright1 8
#define logicright2 10
#define pwm1 11
//Left motor
#define logicleft1 7
#define logicleft2 12
#define pwm2 6

int pwm;
int logic1;
int logic2;

int buffer[2];

void setup(){
 pinMode(pwm1, OUTPUT);
 pinMode(logicleft1, OUTPUT);
 pinMode(logicleft2, OUTPUT);

 pinMode(pwm2, OUTPUT);
 pinMode(logicright1, OUTPUT);
 pinMode(logicright2, OUTPUT);
 
 Serial.begin(9600);
}

void loop(){
 if(Serial.available()>1){
   for(int i=0;i<2;i++){
     buffer[i] = Serial.read()-48;
   }
   if(buffer[1] == 0){
     stop(buffer[0]);
   }
   else{
     move(buffer[0], buffer[1], 100);
   }
 }  
}

void move(int motor, int way, int power){
 if(motor == 0){//right
   pwm = pwm1;
   logic1 = logicleft1;
   logic2 = logicleft2;
 }
 if(motor == 1){//left
   pwm = pwm2;
   logic1 = logicright1;
   logic2 = logicright2;
 }
 power = map(power,0,100,0,255);
 analogWrite(pwm, power);
 if(way == 1){//backward
   digitalWrite(logic1, LOW);
   digitalWrite(logic2, HIGH);
 }
 if(way == 2){//forward
   digitalWrite(logic1, HIGH);
   digitalWrite(logic2, LOW);
 }
}
void stop(int motor){
 if(motor == 0){//right
   digitalWrite(pwm1, HIGH);
   digitalWrite(logicleft1, HIGH);
   digitalWrite(logicleft2, HIGH);
 }
 if(motor == 1){//left
   digitalWrite(pwm2, HIGH);
   digitalWrite(logicright1, HIGH);
   digitalWrite(logicright2, HIGH);
 }
}
