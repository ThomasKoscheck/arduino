// Relais Steuerpin an Arduino 8
const int relaisPin = 8;  
int sensorPin = 0;
int sensorValue = 0;
 
void setup() {
  pinMode(relaisPin, OUTPUT); 
  Serial.begin(115200);
}
 
void loop() { 
  //Helligkeit messen (großer Wert = dunkel)
  if(analogRead(0) > 65)
  {
    //Relais durchschalten, Licht geht an
    digitalWrite(relaisPin, HIGH);
  }

  else 
  {
    //Licht geht aus
     digitalWrite(relaisPin, LOW);
  }

  delay(1000);
}

